# Stage 1
FROM digilabpublic.azurecr.io/golang:1.22 AS golang_builder

WORKDIR /build
COPY backend/go.* ./

RUN go mod download

COPY backend ./

RUN CGO_ENABLED=0 GOOS=js GOARCH=wasm go build -ldflags="-s -w" -o main.wasm cmd/rego-codegen/rego-codegen.go

# Stage 2
FROM digilabpublic.azurecr.io/node:20-alpine3.19 AS node_builder

RUN corepack enable && corepack prepare pnpm@latest --activate

# Copy the code into the container. Note: copy to a dir instead of `.`, since Parcel cannot run in the root dir, see https://github.com/parcel-bundler/parcel/issues/6578
WORKDIR /build
COPY frontend/package.json frontend/pnpm-lock.yaml ./

RUN pnpm install

COPY --from=golang_builder /build/main.wasm static/main.wasm
COPY frontend .

# Build the static files
RUN pnpm run build


# Stage 3: serve the files using nginx
FROM digilabpublic.azurecr.io/nginx:1.25.3-alpine3.18

COPY --from=node_builder /build/build /usr/share/nginx/html

# COPY ./nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

