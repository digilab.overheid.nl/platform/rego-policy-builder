# Contributing

## Set up your development environment

### Requirements

We use [asdf](https://asdf-vm.com/) for installing [required tools](.tool-versions):
```sh
asdf plugin add golang
asdf plugin add golangci-lint
asdf plugin add pnpm
asdf plugin add pre-commit
asdf install
```

## Running locally

The first time you run the project you need to install all npm dependencies
```sh
make install
```

After that, you can just do:
```sh
make dev
```

Open [localhost:5173](http://localhost:5173) for the landing page.

# Go WASM
For the rego go generator we are using go wasm. To generate a new wasm binary you can run `make generate`.
