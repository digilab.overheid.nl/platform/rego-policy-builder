---
title : "FSC Policy Builder"
description: "Documentation for the FSC Policy Builder"
lead: "Service that helps to build policies for FSC using a GUI."
date: 2024-03-07T16:00:00+02:00
draft: false
toc: true
---

*You can find the repository on [GitLab](https://gitlab.com/digilab.overheid.nl/platform/fsc-policy-builder).*

## Online version

This service is available online at [fsc-policy-builder.apps.digilab.network](https://fsc-policy-builder.apps.digilab.network/).


## Overview

This is a no-code / low-code service that helps to build policies for [FSC](https://docs.fsc.nlx.io/introduction).

FSC NLX is an open source peer-to-peer system that facilitates secure connections between services. [Open Policy Agent](https://www.openpolicyagent.org/) can be enabled for FSC to handle request using policies.

Such policies are typically written in the [Rego](https://www.openpolicyagent.org/docs/latest/policy-language/) programming language. However, writing such policies can be difficult. This FSC Policy Builder helps to build policies (currently only for OPA/Rego), making it easier to enable policies on FSC.


## Technical details

The service is deployed as fully static HTML/JS/CSS. Code is evaluated in the frontend using [Go WebAssembly](https://github.com/golang/go/wiki/WebAssembly).

The user's input and data is stored in the browser's `localStorage`.

See the [CONTRIBUTING guide](https://gitlab.com/digilab.overheid.nl/platform/fsc-policy-builder/-/blob/main/CONTRIBUTING.md) for more details.
