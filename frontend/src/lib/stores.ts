import { writable } from 'svelte/store';
import { persisted } from 'svelte-persisted-store';
import type { Route, Policy, PlaygroundInput } from '$lib';

// Define stores
export const successMessage = writable('');
export const errorMessage = writable('');
export const flashMessage = persisted<string>('flashMessage', ''); // Note: a flash message is stored in localStorage and shown on the next page (currently only /policies)
export const routes = persisted<Route[]>('routes', []);
export const policies = persisted<Policy[]>('policies', []);
export const data = persisted<object>('data', { abc: true });
export const input = persisted<PlaygroundInput>('input', {
  method: 'GET',
  path: '',
  requestHeaders: [],
});
