export type Route = {
  method:
    | 'GET'
    | 'POST'
    | 'PUT'
    | 'DELETE'
    | 'PATCH'
    | 'HEAD'
    | 'OPTIONS'
    | 'TRACE'
    | 'CONNECT'
    | 'ANY';
  path: string;
  policyId: number | undefined;
};

// JSONValue is used for arbitrary JSON values
export type JSONValue =
  | string
  | number
  | boolean
  | null
  | JSONValue[]
  | { [key: string]: JSONValue };

export type ConstantBody = JSONValue;

// UnaryBody is used for bodies of unary operators (operators with only one argument, e.g. 'not')
export type UnaryBody = Expression;

// BinaryBody is used for bodies of binary operators (operators with two arguments, e.g. '==')
export type BinaryBody = {
  left: Expression;
  right: Expression;
};

// ListBody is used for bodies of operators that take multiple other expressions as arguments, e.g. 'and'
export type ListBody = Expression[];

export type ExpressionBody = ConstantBody | UnaryBody | BinaryBody | ListBody;

export type ExpressionOperator =
  | 'always'
  | 'constant'
  | 'input'
  | 'data'
  | 'and'
  | 'or'
  | 'not'
  | '=='
  | '!='
  | 'in'
  | '<'
  | '<='
  | '>'
  | '>='
  | '+'
  | '-'
  | '*'
  | 'contains'
  | 'containsAll'
  | 'matchesRegExp';

export type Expression = {
  operator: ExpressionOperator;
  body: ExpressionBody;
};

export type Rule = {
  condition: Expression;
  effect: 'allow' | 'deny';
};

export type Policy = {
  id: number;
  name: string;
  rules: Rule[];
};

export type PlaygroundInput = {
  method:
    | 'GET'
    | 'POST'
    | 'PUT'
    | 'DELETE'
    | 'PATCH'
    | 'HEAD'
    | 'OPTIONS'
    | 'TRACE'
    | 'CONNECT'
    | 'ANY';
  path: string;
  requestHeaders: { key: string; value: string }[];
  fscPeerId?: string; // Note: the FSC peer ID is a string (instead of integer), according to e.g. https://docs.fsc.nlx.io/try-fsc-nlx/helm/11-access-api/
};

export function focusElement(el: HTMLElement) {
  el.focus();
}
