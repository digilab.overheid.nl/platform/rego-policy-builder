import type { SuggestionProps, SuggestionKeyDownProps } from '@tiptap/suggestion';
import SuggestionList from '$lib/SuggestionList.svelte';

export default {
  // See https://tiptap.dev/docs/editor/api/utilities/suggestion for available options
  char: '$',
  allowedPrefixes: null, // Allow any prefix (default is space)
  decorationClass: 'variable', // The class of the element while typing

  items: ({ query }: { query: string }) => {
    return ['fscPeerId', 'requestHeaders', 'queryParameters', 'routeParameters']
      .filter((item) => item.toLowerCase().startsWith(query.toLowerCase()))
      .slice(0, 5);
  },

  render: () => {
    let component: SuggestionList, wrapper: HTMLDivElement;

    return {
      onStart: (props: SuggestionProps) => {
        const { editor } = props;

        wrapper = document.createElement('div');
        wrapper.className = 'variable-suggestions';
        editor.view.dom.parentNode?.appendChild(wrapper);

        component = new SuggestionList({
          target: wrapper,
          props: { props },
        });

        // if (!props.clientRect) {
        //   return
        // }

        // popup = tippy('body', {
        //   getReferenceClientRect: props.clientRect,
        //   appendTo: () => document.body,
        //   content: component.element,
        //   showOnCreate: true,
        //   interactive: true,
        //   trigger: 'manual',
        //   placement: 'bottom-start',
        // })
      },

      onUpdate(props: SuggestionProps) {
        // if (!props.clientRect) {
        //   return
        // }

        component?.$set({ props });
      },

      onKeyDown(props: SuggestionKeyDownProps) {
        if (props.event.key === 'Escape') {
          return true;
        }

        // return component.onKeyDown(props);
        return false;
      },

      onExit() {
        component?.$destroy();
        wrapper.remove();
      },
    };
  },
};
