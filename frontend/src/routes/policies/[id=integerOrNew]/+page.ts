import { error } from '@sveltejs/kit';

/** @type {import('./$types').PageLoad} */
export function load({ params }) {
  if (params.id === 'new') {
    return {
      id: params.id,
    };
  }

  const id = parseInt(params.id, 10);

  if (id !== undefined) {
    return {
      id,
    };
  }

  error(404, 'Policy niet gevonden');
}
