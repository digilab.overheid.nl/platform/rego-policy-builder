
PROJECT_NAME := fsc-policy-builder
ROOT_DIR := ${PWD}

.PHONY: install
install:
	cd frontend && pnpm install

.PHONY: dev
dev:
	cd frontend && pnpm run dev

.PHONY: generate
generate:
	cd backend && GOOS=js GOARCH=wasm go build -o main.wasm cmd/rego-codegen/rego-codegen.go && mv main.wasm ../frontend/static/
