package main

import (
	"context"
	"fmt"
	"testing"

	"os"

	"github.com/open-policy-agent/opa/rego"
	"github.com/open-policy-agent/opa/storage/inmem"
	"github.com/stretchr/testify/assert"
	"gitlab.com/digilab.overheid.nl/platform/fsc-policy-builder/backend/pkg/codegen"
	"gitlab.com/digilab.overheid.nl/platform/fsc-policy-builder/backend/pkg/codegen/model"
)

func toPtr[T any](v T) *T {
	return &v
}

func Test(t *testing.T) {
	routes := []model.Route{
		{
			PolicyId: 1,
			Method:   "GET",
			Path:     "/test/*",
		},
		{
			PolicyId: 1,
			Method:   "POST",
			Path:     "/test/abc",
		},
	}

	policies := []model.Policy{
		{
			ID:   1,
			Name: "TestPolicy",
			Rules: []model.Rule{
				{
					Effect: model.EffectAllow,
					Expression: model.Expression{
						Body: model.ExpressionBody{
							Binary: &model.BinaryBody{
								Right: model.Expression{
									Body: model.ExpressionBody{
										Constant: &model.ConstantBody{
											Number: toPtr(int64(123)),
										},
									},
									Operator: "constant",
								},
								Left: model.Expression{
									Body: model.ExpressionBody{
										Constant: &model.ConstantBody{
											String: toPtr("blah"),
										},
									},
									Operator: "data",
								},
							},
						},
						Operator: "==",
					},
				},
				{

					Effect: model.EffectDeny,
					Expression: model.Expression{
						Body: model.ExpressionBody{
							Binary: &model.BinaryBody{
								Right: model.Expression{
									Body: model.ExpressionBody{
										Constant: &model.ConstantBody{
											Number: toPtr(int64(1234)),
										},
									},
									Operator: "constant",
								},
								Left: model.Expression{
									Body: model.ExpressionBody{
										Constant: &model.ConstantBody{
											String: toPtr("blah2"),
										},
									},
									Operator: "data",
								},
							},
						},
						Operator: "==",
					},
				},
				{
					Effect: model.EffectAllow,
					Expression: model.Expression{
						Body: model.ExpressionBody{
							Binary: &model.BinaryBody{
								Right: model.Expression{
									Body: model.ExpressionBody{
										Constant: &model.ConstantBody{
											List: &[]model.ConstantBody{
												{
													Number: toPtr(int64(1)),
												},
												{
													Number: toPtr(int64(2)),
												},
												{
													Number: toPtr(int64(3)),
												},
											},
										},
									},
									Operator: "constant",
								},
								Left: model.Expression{
									Body: model.ExpressionBody{
										Constant: &model.ConstantBody{
											Number: toPtr(int64(3)),
										},
									},
									Operator: "constant",
								},
							},
						},
						Operator: "contains",
					},
				},
			},
		},
	}

	input := map[string]any{
		"method": "GET",
		"path":   "/test/123",
	}

	data := map[string]any{
		"blah":  123,
		"blah2": 1234,
	}

	opts := codegen.Opts{
		PackageName: "test",
		Routes:      routes,
		Policies:    policies,
	}

	module, err := codegen.Generate(opts)
	if err != nil {
		t.Fatalf("generation: %v", err)
	}

	fmt.Fprintf(os.Stdout, "rego: \n%+v\n", []any{module}...)

	ctx := context.TODO()

	// Manually create the storage layer. inmem.NewFromObject returns an
	// in-memory store containing the supplied data.
	store := inmem.NewFromObject(data)

	query, err := rego.New(
		rego.Query("data.allow"),
		rego.Module("rego", module),
		rego.Input(input),
		rego.Store(store),
	).PrepareForEval(ctx)

	if err != nil {
		assert.Fail(t, "rego new failed:", err)
		return
	}

	results, err := query.Eval(ctx)
	if err != nil {
		assert.Fail(t, "query eval failed:", err)
		return
	}

	if len(results) == 0 {
		fmt.Fprintf(os.Stdout, "no results")
		// Handle undefined result.
	} else {
		fmt.Fprintf(os.Stdout, "%+v\n", results[0].Expressions[0].Value.(bool))
		// Handle result/decision.
	}
}
