package main

import (
	"bytes"
	"compress/gzip"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/url"
	"strings"
	"syscall/js"

	"github.com/open-policy-agent/opa/cover"
	"github.com/open-policy-agent/opa/rego"
	"github.com/open-policy-agent/opa/storage/inmem"
	"github.com/open-policy-agent/opa/topdown"
	"gitlab.com/digilab.overheid.nl/platform/fsc-policy-builder/backend/pkg/codegen"
	"gitlab.com/digilab.overheid.nl/platform/fsc-policy-builder/backend/pkg/codegen/model"
)

func main() {
	js.Global().Set("generateRego", js.FuncOf(generateRego))
	js.Global().Set("runRego", js.FuncOf(runRego))
	select {}
}

const (
	Header               = "#### GENERATED CODE HEADER. ####"
	EncodedDataLength    = 30
	EncodedDataSeperator = "\n# "
)

func generateRego(this js.Value, args []js.Value) interface{} {
	arg := args[0]

	var arguments struct {
		Routes   []model.Route  `json:"routes"`
		Policies []model.Policy `json:"policies"`
	}

	if err := json.Unmarshal([]byte(arg.String()), &arguments); err != nil {
		return fmt.Sprintf("arguments unmarshal failed: %s", err)
	}

	opts := codegen.Opts{
		PackageName: "rego",
		Routes:      arguments.Routes,
		Policies:    arguments.Policies,
	}

	rego, err := codegen.Generate(opts)
	if err != nil {
		return fmt.Sprintf("codegen failed: %s", err)
	}

	encodedArgs, err := encodeArgs([]byte(arg.String()))
	if err != nil {
		return fmt.Sprintf("encode args: %s", err)
	}

	rego = writeExportData(rego, encodedArgs)

	return strings.TrimSpace(rego.String())
}

func writeExportData(w *strings.Builder, data []byte) *strings.Builder {
	w.WriteString("\n\n")
	w.WriteString(Header)

	for i := 0; i < len(data); i += EncodedDataLength {
		w.WriteString(EncodedDataSeperator)

		end := i + EncodedDataLength
		if end > len(data) {
			end = len(data)
		}

		w.Write(data[i:end])
	}

	return w
}

func encodeArgs(data []byte) ([]byte, error) {
	var b bytes.Buffer
	gz := gzip.NewWriter(&b)
	if _, err := gz.Write(data); err != nil {
		return nil, fmt.Errorf("gzip write: %w", err)
	}

	if err := gz.Close(); err != nil {
		return nil, fmt.Errorf("gzip close: %w", err)
	}

	res := make([]byte, base64.StdEncoding.EncodedLen(len(b.Bytes())))
	base64.StdEncoding.Encode(res, b.Bytes())
	return res, nil
}

// runRego evaluates the data and returns a JSON marshalled result or error string. Note: the `any` return type is required by js.FuncOf
func runRego(this js.Value, args []js.Value) any {
	// Get the data from the function argument
	arg := args[0]

	var arguments struct {
		Routes   []model.Route  `json:"routes"`
		Policies []model.Policy `json:"policies"`
		Data     map[string]any `json:"data"`
		Input    map[string]any `json:"input"`
	}
	if err := json.Unmarshal([]byte(arg.String()), &arguments); err != nil {
		return fmt.Sprintf("arguments unmarshal failed: %s", err)
	}

	// Parse the policy routes
	type parsedRoute struct {
		Method string
		URL    *url.URL
	}

	parsedRoutes := make([]parsedRoute, 0, len(arguments.Routes))
	for _, route := range arguments.Routes {
		if parsed, err := url.Parse(route.Path); err == nil {
			parsedRoutes = append(parsedRoutes, parsedRoute{
				Method: route.Method,
				URL:    parsed,
			})
		}
	}

	// Generate the Rego module
	module, err := codegen.Generate(codegen.Opts{
		PackageName: "rego",
		Routes:      arguments.Routes,
		Policies:    arguments.Policies,
	})
	if err != nil {
		return fmt.Sprintf("codegen failed: %s", err)
	}

	r := rego.New(
		rego.Query("data.rego.allow"),
		rego.Module("rego", strings.TrimSpace(module.String())),
		rego.Input(arguments.Input),
		rego.Store(inmem.NewFromObject(arguments.Data)),
	)

	ctx := context.Background()

	// Prepare the Rego object for evaluation, in order to get its modules
	pq, err := r.PrepareForEval(ctx)
	if err != nil {
		return fmt.Sprintf("preparation for evaluation failed: %s", err)
	}

	// Initialize two tracers to show more information to the user
	coverTracer := cover.New()
	eventTracer := topdown.NewBufferTracer()

	res, err := pq.Eval(ctx, rego.EvalQueryTracer(coverTracer), rego.EvalTracer(eventTracer))
	if err != nil {
		return fmt.Sprintf("query evaluation failed: %s", err)
	}

	report := coverTracer.Report(pq.Modules())

	buf := new(bytes.Buffer)
	topdown.PrettyTraceWithLocation(buf, *eventTracer)

	// Return the results
	results := struct {
		IsAllowed bool              `json:"isAllowed"`
		Module    string            `json:"module"`
		Coverage  *cover.FileReport `json:"coverage"`
		Trace     string            `json:"trace"`
	}{
		res.Allowed(),
		strings.TrimSpace(module.String()),
		report.Files["rego"],
		buf.String(),
	}

	data, err := json.Marshal(results)
	if err != nil {
		return fmt.Sprintf("JSON marhsalling failed: %s", err)
	}

	return string(data)
}

// matchWithWildcard returns whether or not the specified string matches the specified pattern with wilcards. Note: filepath.Match can also be used, but that matches several other characters, which is undesired
func matchWithWildcard(pattern, str string) (bool, []string) {
	// If the pattern does not contain wildcards, return with a simple string comparison
	if !strings.Contains(pattern, "*") {
		return str == pattern, nil
	}

	// Split the pattern into segments separated by '*'
	segments := strings.Split(pattern, "*")

	// Initialize start index to 0
	startIdx := 0

	// Initialize a slice to store the values of the matched wildcards
	var wildcards []string

	// Iterate over each segment of the pattern
	for _, segment := range segments {
		// Find the index of the current segment in the string
		idx := strings.Index(str[startIdx:], segment)
		if idx == -1 {
			// If the segment is not found, return false and an empty slice
			return false, nil
		}

		// For the first segment, check if the pattern starts with a wildcard
		currentSegment := str[startIdx : startIdx+idx]
		if startIdx != 0 || strings.HasPrefix(str, "*") {
			// Add the substring between the wildcard segments to the wildcards slice
			wildcards = append(wildcards, currentSegment)
		} else if startIdx == 0 && currentSegment != "" {
			return false, nil
		}

		// Move the start index to the end of the found segment
		startIdx += idx + len(segment)
	}

	// Get the end segment
	endSegment := str[startIdx:]

	// If the end segment is not empty, make sure the pattern ends with a wildcard
	if strings.HasSuffix(str, "*") {
		// Add the segment to the wildcards slice
		wildcards = append(wildcards, endSegment)
	} else if endSegment != "" {
		return false, nil
	}

	// If all segments are found in the correct order, return true and the values of the matched wildcards
	return true, wildcards
}
