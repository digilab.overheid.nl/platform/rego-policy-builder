package codegen

import (
	"bufio"
	"bytes"
	"embed"
	"fmt"
	"io/fs"
	"regexp"
	"strings"
	"text/template"

	"gitlab.com/digilab.overheid.nl/platform/fsc-policy-builder/backend/pkg/codegen/model"
)

//go:embed templates
var templates embed.FS

type Opts struct {
	PackageName string
	Routes      []model.Route
	Policies    []model.Policy
	// Data        map[string]any
}

func Generate(opts Opts) (*strings.Builder, error) {
	t := template.New("fsc-codegen").Funcs(template.FuncMap{
		"contains":    strings.Contains,
		"replace":     strings.ReplaceAll,
		"deref":       derefString,
		"path2regExp": path2regExp,
	})

	if err := LoadTemplates(templates, t); err != nil {
		return nil, fmt.Errorf("error parsing templates: %w", err)
	}

	var buf strings.Builder

	imp, err := GenerateImports(t, opts)
	if err != nil {
		return nil, fmt.Errorf("generating imports failed: %w", err)
	}

	if _, err := buf.WriteString(imp); err != nil {
		return nil, fmt.Errorf("writing imports failed: %w", err)
	}

	policies := map[int]model.Policy{}
	for _, p := range opts.Policies {
		policies[p.ID] = p
	}

	policy, err := GenerateRoute(t, opts.Routes, policies)
	if err != nil {
		return nil, fmt.Errorf("generating policies failed: %w", err)
	}

	if _, err := buf.WriteString(policy); err != nil {
		return nil, fmt.Errorf("writing policies failed: %w", err)
	}

	return &buf, nil
}

// LoadTemplates loads all of our template files into a text/template. The
// path of template is relative to the templates directory.
func LoadTemplates(src embed.FS, t *template.Template) error {
	return fs.WalkDir(src, "templates", func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return fmt.Errorf("error walking directory %s: %w", path, err)
		}
		if d.IsDir() {
			return nil
		}

		buf, err := src.ReadFile(path)
		if err != nil {
			return fmt.Errorf("error reading file '%s': %w", path, err)
		}

		templateName := strings.TrimPrefix(path, "templates/")
		tmpl := t.New(templateName)
		_, err = tmpl.Parse(string(buf))
		if err != nil {
			return fmt.Errorf("parsing template '%s': %w", path, err)
		}
		return nil
	})
}

func GenerateImports(t *template.Template, opts Opts) (string, error) {
	hasAllow := false
	hasDeny := false

	policies := map[int]model.Policy{}
	for _, policy := range opts.Policies {
		policies[policy.ID] = policy
	}

	for _, route := range opts.Routes {
		policy, ok := policies[route.PolicyId]
		if !ok {
			continue
		}

		if policy.HasAllowRules() {
			hasAllow = true
		}

		if policy.HasDenyRules() {
			hasDeny = true
		}
	}

	context := struct {
		PackageName string
		HasAllow    bool
		HasDeny     bool
	}{
		PackageName: opts.PackageName,
		HasAllow:    hasAllow,
		HasDeny:     hasDeny,
	}

	return GenerateTemplates([]string{"imports.tmpl"}, t, context)
}

// derefString converts a *string to a string
func derefString(s *string) string {
	if s != nil {
		return *s
	}

	return ""
}

// path2regExp converts a route path to a regular expression, useful for wildcard matching
func path2regExp(path string) string {
	var routeRE strings.Builder
	routeRE.WriteByte('^')

	lastWrittenIndex := 0
	for i := 0; i < len(path); i++ { // Note: instead of using `range`, which loops over the runes in a string
		if path[i] == '*' {
			routeRE.WriteString(regexp.QuoteMeta(path[lastWrittenIndex:i]))
			routeRE.WriteString("(.*)")
			lastWrittenIndex = i + 1
		}
	}

	routeRE.WriteString(regexp.QuoteMeta(path[lastWrittenIndex:]))
	routeRE.WriteByte('$')

	return routeRE.String()
}

// GenerateTemplates used to generate templates
func GenerateTemplates(templates []string, t *template.Template, ops interface{}) (string, error) {
	var generatedTemplates []string
	for _, tmpl := range templates {
		var buf bytes.Buffer
		w := bufio.NewWriter(&buf)

		if err := t.ExecuteTemplate(w, tmpl, ops); err != nil {
			return "", fmt.Errorf("error generating %s: %s", tmpl, err)
		}
		if err := w.Flush(); err != nil {
			return "", fmt.Errorf("error flushing output buffer for %s: %s", tmpl, err)
		}
		generatedTemplates = append(generatedTemplates, buf.String())
	}

	return strings.Join(generatedTemplates, "\n"), nil
}

func GenerateRoute(t *template.Template, routes []model.Route, policies map[int]model.Policy) (string, error) {
	var generatedPolicies []string
	for idx := range routes {
		ctx := struct {
			Route  model.Route
			Policy model.Policy
		}{
			Route:  routes[idx],
			Policy: policies[routes[idx].PolicyId],
		}

		generatedPolicy, err := GenerateTemplates([]string{"policy.tmpl"}, t, ctx)
		if err != nil {
			return "", err
		}

		generatedPolicies = append(generatedPolicies, generatedPolicy)
	}

	return strings.Join(generatedPolicies, "\n"), nil
}

// GeneratePolicy used to generate policies
func GeneratePolicy(t *template.Template, policies []model.Policy) (string, error) {
	var generatedPolicies []string
	for idx := range policies {
		context := struct {
			Policy model.Policy
			Route  model.Route
		}{
			Policy: policies[idx],
		}

		generatedPolicy, err := GenerateTemplates([]string{"policy.tmpl"}, t, context)
		if err != nil {
			return "", err
		}

		generatedPolicies = append(generatedPolicies, generatedPolicy)
	}

	return strings.Join(generatedPolicies, "\n"), nil
}
