package model

type Policy struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Rules []Rule `json:"rules"`
}

func (policy Policy) HasAllowRules() bool {
	for _, r := range policy.Rules {
		if r.Effect == EffectAllow {
			return true
		}
	}

	return false
}

func (policy Policy) HasDenyRules() bool {
	for _, r := range policy.Rules {
		if r.Effect == EffectDeny {
			return true
		}
	}

	return false
}
