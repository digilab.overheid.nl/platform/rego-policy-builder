package model

type Route struct {
	Method   string `json:"method"`
	Path     string `json:"path"`
	PolicyId int    `json:"policyId"`
}
