package model

import (
	"encoding/json"
	"errors"
)

type RuleEffect string
type ExpressionOperators string

const (
	EffectAllow RuleEffect = "allow"
	EffectDeny  RuleEffect = "deny"
)

type Rule struct {
	Effect     RuleEffect `json:"effect"`
	Expression Expression `json:"condition"`
}

type Expression struct {
	Body     ExpressionBody      `json:"body"`
	Operator ExpressionOperators `json:"operator"`
}

func (Exp Expression) IsEmpty() bool {
	return !(Exp.Body.Constant != nil || Exp.Body.Binary != nil || Exp.Body.Unary != nil || Exp.Body.List != nil)
}

type ConstantBody struct {
	String  *string
	Number  *int64
	Boolean *bool
	List    *[]ConstantBody
	Map     *map[string]ConstantBody
}

func (constant *ConstantBody) UnmarshalJSON(data []byte) error {
	s := new(string)
	if err := json.Unmarshal(data, s); err == nil {
		constant.String = s
		return nil
	}
	i64 := new(int64)
	if err := json.Unmarshal(data, i64); err == nil {
		constant.Number = i64
		return nil
	}
	b := new(bool)
	if err := json.Unmarshal(data, b); err == nil {
		constant.Boolean = b
		return nil
	}
	lcb := new([]ConstantBody)
	if err := json.Unmarshal(data, lcb); err == nil {
		constant.List = lcb
		return nil
	}
	mcb := new(map[string]ConstantBody)
	if err := json.Unmarshal(data, mcb); err == nil {
		constant.Map = mcb
		return nil
	}

	return errors.New("constant body not found")
}

type UnaryBody Expression

func (ub *UnaryBody) IsEmpty() bool {
	return (*Expression)(ub).IsEmpty()
}

type BinaryBody struct {
	Left  Expression
	Right Expression
}

type ListBody []Expression

type ExpressionBody struct {
	Constant *ConstantBody
	Unary    *UnaryBody
	Binary   *BinaryBody
	List     *ListBody
}

func (expBody *ExpressionBody) UnmarshalJSON(data []byte) error {
	bb := new(BinaryBody)
	if err := json.Unmarshal(data, bb); err == nil {
		expBody.Binary = bb
		return nil
	}

	lb := new(ListBody)
	if err := json.Unmarshal(data, lb); err == nil {
		expBody.List = lb
		return nil
	}

	ub := new(UnaryBody)
	if err := json.Unmarshal(data, ub); err == nil && ub.IsEmpty() {
		expBody.Unary = ub
		return nil
	}

	cb := new(ConstantBody)
	if err := json.Unmarshal(data, cb); err == nil {
		expBody.Constant = cb
		return nil
	}

	return errors.New("expression body not found")
}
